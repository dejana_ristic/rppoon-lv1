﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON
{
    class Note
    {
        private String text;
        private String author;
        private int priority;

        public string getText() { return this.text; }
        public string getAuthor() { return this.author; }
        public int getPriority() { return this.priority; }
        public void setText(string Text) { this.text = Text; }
        public void setAuthor(string Author) { this.author = Author; }
        public void setPriority(int Priority) { this.priority = Priority; }

        public Note()
        {
            text = "abc";
            author = "NN";
            priority = 1;
        }
        public Note(string text)
        {
            this.text = text;
            this.author = "NN";
            this.priority = 1;
        }
        public Note(string text, string author, int priority)
        {
            this.text = text;
            this.author = author;
            this.priority = priority;
        }

        public string Text
        {
            get { return this.text; }
            set { this.text = value; }
        }
        public string Author
        {
            get { return this.author; }
            set { this.text = value; }
        }
        public int Property
        {
            get { return this.priority; }
            set { this.priority = value; }
        }
        public override string ToString()
        {
            return this.author + " : " + this.text;
        }
    }
}
