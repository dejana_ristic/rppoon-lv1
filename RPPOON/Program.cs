﻿using System;

namespace RPPOON
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note;
            note = new Note();
            Console.WriteLine(note.getAuthor());
            note = new Note("Operi veš!", "Mama", 1);
            Console.WriteLine(note.getAuthor());
            Console.WriteLine("Autor: ");
            string author = Console.ReadLine();
            note.setAuthor(author);
            Console.WriteLine(note.getAuthor());

            Console.WriteLine(note.ToString());
            Console.WriteLine(note);

            Console.WriteLine(note.ToString());
            Time_reference time_reference = new Time_reference();
            Console.WriteLine(time_reference.ToString());
            Note noteRef = new Time_reference();
            Console.WriteLine(noteRef.ToString());
        }
    }
}
