﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON
{
    class Time_reference:Note 
    {
        private DateTime time;
        public Time_reference() : base() {
            time = DateTime.Now;
        }
        public Time_reference(string text, string author, int priority) : base(text, author, priority) {
            this.time = DateTime.Now;
        }
        public Time_reference(string text, string author, int priority, DateTime time) : base(text, author, priority) {
            this.time = time; 
        }
        public DateTime Time {
            get { return this.time; }
            set { this.time = value; }
        }
        public override string ToString()
        {
            return base.ToString() + "\n" + this.Time;
        }

    }
}
